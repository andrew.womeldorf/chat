#!/usr/bin/env sh

# Don't set -x yet, so as not to print the matrix key to the logs
set -e

# Save the matrix key from the environment variable
echo "${MATRIX_KEY}" | base64 -d > /etc/dendrite/matrix_key.pem

set -x

LITESTREAM_CONFIG=/etc/litestream.yml

# Create the config file from environment variables
yq '(.. | select(tag == "!!str")) |= envsubst' dendrite.example.yaml > dendrite.yaml

# Pull databases from external storage, if not exists
for i in $(cat $LITESTREAM_CONFIG | yq .dbs[].path); do
    if [ -f $i ]; then
        echo "Database already exists, skipping restore:  $i"
    else
        echo "No database found, restoring from replica if exists:  $i"
        mkdir -p $(dirname $i)
        litestream restore -v -if-replica-exists -config $LITESTREAM_CONFIG $i
    fi
done

# Start replicating the databases back to external storage and begin the Dendrite process
exec litestream replicate -config $LITESTREAM_CONFIG -exec "/usr/bin/dendrite-monolith-server"
