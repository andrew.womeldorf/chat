# Dendrite Matrix Server

Inspired by
[https://codeberg.org/gerald/dendrite-on-flyio](https://codeberg.org/gerald/dendrite-on-flyio)

It's a very similar deployment, but adds Litestream to backup the database files.

## Config file

Modify `dendrite.example.yaml` to fit your needs. Some references are [the
config from the Dendrite
repository](https://github.com/matrix-org/dendrite/blob/main/dendrite-config.yaml)
and the config file [from this other reference deployment to
fly.io](https://codeberg.org/gerald/dendrite-on-flyio).

Values encased in `${SOME_KEY}` will be replaced when the container starts by
the value in a matching key as an environment variable. For example:

```bash
export SERVER_NAME=chat.foobar.com
```

```yaml
global:
  server_name: ${SERVER_NAME}
```

will evaluate at runtime to:

```yaml
global:
  server_name: chat.foobar.com
```

## Key

Create a key. This should stay the same key for all restarts of the container,
so make sure it's saved somewhere safe!

```
docker run --rm -it -v $(pwd):/key -w /key --entrypoint /usr/bin/generate-keys matrixdotorg/dendrite-monolith:latest --private-key matrix_key.pem
```

Base64 Encode the key (removing newlines) and set it as a secret in fly:

```bash
base64 -w 0 matrix_key.pem | fly secrets set MATRIX_KEY=-
```

This will be decoded and saved to the filesystem on startup.

## Deploy

Litestream may need credentials. In my case (Wasabi), it takes environment
variables with an access key and secret access key. I set them as secrets:

```bash
fly secrets set AWS_ACCESS_KEY_ID=... AWS_SECRET_ACCESS_KEY=...
```

**Update the fly.toml!!!** Most if it should be reusable, but in particular,
there are some things in the `[[env]]` section that must change per deployment.

Then, you should be able to deploy:

```bash
fly deploy
```

## Domain

Get the IP addresses for your deployment, and set them in your DNS.

```bash
fly info
```

Then obtain a TLS certificate:

```
fly certs add <hostname>
```
