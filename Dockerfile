FROM docker.io/litestream/litestream:0.3.8 as litestream
FROM docker.io/mikefarah/yq:4.22.1 as yq

FROM docker.io/matrixdotorg/dendrite-monolith:v0.12.0
COPY --from=litestream /usr/local/bin/litestream /usr/local/bin/litestream
COPY --from=yq /usr/bin/yq /usr/local/bin/yq
COPY entrypoint.sh /scripts/entrypoint.sh
COPY litestream.yml /etc/litestream.yml
COPY dendrite.example.yaml /etc/dendrite/dendrite.example.yaml

ENTRYPOINT ["/scripts/entrypoint.sh"]
